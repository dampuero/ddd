
namespace Mo.Dominio.IServicios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;
    using System.Collections.Generic;

    public interface MoTwo_ITableTest1Service : IService<MoTwo_TableTest1>
    {
        //Generate test Method 
        MoTwo_TableTest1 getCustom(Guid id);
    }
}
