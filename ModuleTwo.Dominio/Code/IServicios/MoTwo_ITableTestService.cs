
namespace Mo.Dominio.IServicios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;
    using System.Collections.Generic;

    public interface MoTwo_ITableTestService : IService<MoTwo_TableTest>
    {
        //Generate test Method 
        MoTwo_TableTest getCustom(Guid id);
    }
}
