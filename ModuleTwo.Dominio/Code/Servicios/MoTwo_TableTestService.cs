 
namespace Mo.Dominio.Servicios
{
    using System;
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IServicios;
    using Mo.Dominio.IRepositorios;

    public class MoTwo_TableTestService
        : ServiceAbstract<MoTwo_TableTest>, MoTwo_ITableTestService
    {
        private readonly MoTwo_ITableTestRepository _repository;

        public MoTwo_TableTestService(MoTwo_ITableTestRepository repository)
        {
            _repository = repository;
            setRepository(_repository);
        }
        
        //Generate test Method 
        public MoTwo_TableTest getCustom(Guid id)
        {
            return _repository.Get(id);
        }
    }
}
