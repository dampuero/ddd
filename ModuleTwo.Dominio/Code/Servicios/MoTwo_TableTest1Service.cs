 
namespace Mo.Dominio.Servicios
{
    using System;
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IServicios;
    using Mo.Dominio.IRepositorios;

    public class MoTwo_TableTest1Service
        : ServiceAbstract<MoTwo_TableTest1>, MoTwo_ITableTest1Service
    {
        private readonly MoTwo_ITableTest1Repository _repository;

        public MoTwo_TableTest1Service(MoTwo_ITableTest1Repository repository)
        {
            _repository = repository;
            setRepository(_repository);
        }
        
        //Generate test Method 
        public MoTwo_TableTest1 getCustom(Guid id)
        {
            return _repository.Get(id);
        }
    }
}
