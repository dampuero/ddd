
namespace Mo.Dominio.IRepositorios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;

    public interface MoTwo_ITableTestRepository : IRepository<MoTwo_TableTest>
    {
        //Generate test Method 
        MoTwo_TableTest getCustom(Guid id);
    }
}
          