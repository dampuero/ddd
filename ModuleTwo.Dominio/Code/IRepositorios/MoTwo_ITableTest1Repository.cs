
namespace Mo.Dominio.IRepositorios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;

    public interface MoTwo_ITableTest1Repository : IRepository<MoTwo_TableTest1>
    {
        //Generate test Method 
        MoTwo_TableTest1 getCustom(Guid id);
    }
}
          