
namespace Mo.Dominio.IServicios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;
    using System.Collections.Generic;

    public interface MoOne_ITableUnique1Service : IService<MoOne_TableUnique1>
    {
        //Generate test Method 
        MoOne_TableUnique1 getCustom(Guid id);
    }
}
