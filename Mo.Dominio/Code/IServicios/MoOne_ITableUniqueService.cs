
namespace Mo.Dominio.IServicios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;
    using System.Collections.Generic;

    public interface MoOne_ITableUniqueService : IService<MoOne_TableUnique>
    {
        //Generate test Method 
        MoOne_TableUnique getCustom(Guid id);

        MoOne_TableUnique insertCustom(MoOne_TableUnique entity);
    }
}
