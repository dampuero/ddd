
namespace Mo.Dominio.IRepositorios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;

    public interface MoOne_ITableUniqueRepository : IRepository<MoOne_TableUnique>
    {
        //Generate test Method 
        MoOne_TableUnique getCustom(Guid id);
    }
}
          