
namespace Mo.Dominio.IRepositorios
{
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using System;

    public interface MoOne_ITableUnique1Repository : IRepository<MoOne_TableUnique1>
    {
        //Generate test Method 
        MoOne_TableUnique1 getCustom(Guid id);
    }
}
          