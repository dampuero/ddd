 
namespace Mo.Dominio.Servicios
{
    using System;
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IServicios;
    using Mo.Dominio.IRepositorios;

    public class MoOne_TableUnique1Service
        : ServiceAbstract<MoOne_TableUnique1>, MoOne_ITableUnique1Service
    {
        private readonly MoOne_ITableUnique1Repository _repository;

        public MoOne_TableUnique1Service(MoOne_ITableUnique1Repository repository)
        {
            _repository = repository;
            setRepository(_repository);
        }
        
        //Generate test Method 
        public MoOne_TableUnique1 getCustom(Guid id)
        {
            return _repository.Get(id);
        }
    }
}
