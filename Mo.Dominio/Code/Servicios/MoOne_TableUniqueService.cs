 
namespace Mo.Dominio.Servicios
{
    using System;
    using Core.Dominio;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IServicios;
    using Mo.Dominio.IRepositorios;

    public class MoOne_TableUniqueService
        : ServiceAbstract<MoOne_TableUnique>, MoOne_ITableUniqueService
    {
        private readonly MoOne_ITableUniqueRepository _repository;

        public MoOne_TableUniqueService(MoOne_ITableUniqueRepository repository)
        {
            _repository = repository;
            setRepository(_repository);
        }
        
        //Generate test Method 
        public MoOne_TableUnique getCustom(Guid id)
        {
            return _repository.Get(id);
        }

        public MoOne_TableUnique insertCustom(MoOne_TableUnique entity)
        {
            // En este caso se debe retornar null porque no se pudo completar la operacion
            // o se debe lanzar una excepcion?????
            // Este if representa logica del dominio, porque para no permitir dato3 duplicados
            // el usuario debio solicitarlo. Entonces si es logica del dominio debe poder informarse 
            // a los usuarios del sistema que no se completo la operacion y explicarles la razon.
            // Si solo retornamos NULL no podremos decifrar esto, entonces lo mas adecuado parace ser
            // que lancemos una excepcion personalizada. 
            // Pero es algo que sabemos que puede suceder, por lo que esa es una causa por la que no debe
            // ser tratado como excepcion. Lo que podr�amos hacer es tener una enum de errores.
            if (_repository.CountAll(o => o.dato3 == entity.dato3) > 0)
                return null;

            return Insert(entity);
        }
    }
}
