﻿
namespace Core.Dominio.Util
{
    using System;

    public class Fecha
    {
        public static int getFechaActual()
        {
            return DateTime.Now.Millisecond;
        }

        public static int getFechaEmpty()
        {
            return 0;
        }
    }
}
