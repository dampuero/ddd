﻿
namespace ModuloTwo.Aplicacion.Dto
{
    public class MoTwo_TableTestDto
    {
        public string Id { get; set; }
        public int dato1 { get; set; }
        public int dato2 { get; set; }
        public string dato3 { get; set; }
        public string dato4 { get; set; }

        public string RowVersion { get; set; }
    }
}
