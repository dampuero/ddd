
namespace ModuleOne.Persistencia.Repositorios
{
    using System;
    using Core.Persistencia;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IRepositorios;
    using ModuleOne.Persistencia.UnidadDeTrabajo;
    
    public class MoOne_TableUnique1Repository : Repository<MoOne_TableUnique1>, MoOne_ITableUnique1Repository
    {
        public MoOne_TableUnique1Repository(MoOne_UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        //Generate test Method 
        public MoOne_TableUnique1 getCustom(Guid id)
        {
            if (id != Guid.Empty)
                return base.Get(id);
            else
                return null;
        }
    }
}        
           