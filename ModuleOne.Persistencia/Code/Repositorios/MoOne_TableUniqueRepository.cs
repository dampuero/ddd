
namespace ModuleOne.Persistencia.Repositorios
{
    using System;
    using Core.Persistencia;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IRepositorios;
    using ModuleOne.Persistencia.UnidadDeTrabajo;
    
    public class MoOne_TableUniqueRepository : Repository<MoOne_TableUnique>, MoOne_ITableUniqueRepository
    {
        public MoOne_TableUniqueRepository(MoOne_UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        //Generate test Method 
        public MoOne_TableUnique getCustom(Guid id)
        {
            if (id != Guid.Empty)
                return base.Get(id);
            else
                return null;
        }
    }
}        
           