
namespace ModuleOne.Persistencia.UnidadDeTrabajo.Mapeadores
{
    
    using Mo.Dominio.Entidades;
    using System.Data.Entity.ModelConfiguration;
    
    class TableUniqueTypeConfiguration : EntityTypeConfiguration<MoOne_TableUnique>
    {
        public TableUniqueTypeConfiguration()
        {
            //key and properties
            ToTable("table_unique");
            this.HasKey(ba => ba.Id);
            			this.Property(ba => ba.dato1);
			this.Property(ba => ba.dato2);
			this.Property(ba => ba.dato3);
			this.Property(ba => ba.dato4);

            this.Property(ba => ba.RowVersion);
        }
    }
}
