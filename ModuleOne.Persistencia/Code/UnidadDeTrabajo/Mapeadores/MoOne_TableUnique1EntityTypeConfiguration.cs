
namespace ModuleOne.Persistencia.UnidadDeTrabajo.Mapeadores
{
    
    using Mo.Dominio.Entidades;
    using System.Data.Entity.ModelConfiguration;
    
    class TableUnique1TypeConfiguration : EntityTypeConfiguration<MoOne_TableUnique1>
    {
        public TableUnique1TypeConfiguration()
        {
            //key and properties
            ToTable("table_unique1");
            this.HasKey(ba => ba.Id);
            			this.Property(ba => ba.dato1);
			this.Property(ba => ba.dato2);
			this.Property(ba => ba.dato3);
			this.Property(ba => ba.dato4);

            this.Property(ba => ba.RowVersion);
        }
    }
}
