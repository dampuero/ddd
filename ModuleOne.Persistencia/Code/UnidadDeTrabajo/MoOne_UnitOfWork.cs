﻿
namespace ModuleOne.Persistencia.UnidadDeTrabajo
{
    using Core.Persistencia;
    using Mo.Dominio.Entidades;
    using ModuleOne.Persistencia.UnidadDeTrabajo.Mapeadores;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class MoOne_UnitOfWork
        : DbContext, IQueryableUnitOfWork
    {
        public MoOne_UnitOfWork()
            : base("ModuleOne.Persistencia.UnidadDeTrabajo.Mo_UnitOfWork")
        {
            Database.SetInitializer<MoOne_UnitOfWork>(null);
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }


        #region IDbSet Miembros

        IDbSet<MoOne_TableUnique> _tableUnique;
        public IDbSet<MoOne_TableUnique> TableUnique
        {
            get
            {
                if (_tableUnique == null)
                    _tableUnique = base.Set<MoOne_TableUnique>();

                return _tableUnique;
            }
        }

        IDbSet<MoOne_TableUnique1> _tableUnique1;
        public IDbSet<MoOne_TableUnique1> TableUnique1
        {
            get
            {
                if (_tableUnique1 == null)
                    _tableUnique1 = base.Set<MoOne_TableUnique1>();

                return _tableUnique1;
            }
        }

        #endregion

        #region IQueryableUnitOfWork Miembros

        public DbSet<TEntity> CreateSet<TEntity>()
            where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void Attach<TEntity>(TEntity item)
            where TEntity : class
        {
            //attach and set as unchanged
            base.Entry<TEntity>(item).State = System.Data.Entity.EntityState.Unchanged;
        }

        public bool isDetached<TEntity>(TEntity item)
            where TEntity : class
        {
            //attach and set as unchanged
            return base.Entry<TEntity>(item).State == System.Data.Entity.EntityState.Detached;
        }

        public void SetModified<TEntity>(TEntity item)
            where TEntity : class
        {
            //this operation also attach item in object state manager
            base.Entry<TEntity>(item).State = System.Data.Entity.EntityState.Modified;
            base.Entry(item).Property("USER_CREACION").IsModified = false;
            //base.Entry(item).Property(x => x.).IsModified = false;
        }

        public void SetModified<TEntity>(TEntity item,
            /*System.Linq.Expressions.Expression<Func<TEntity, object>>[]*/IList<string> properties)
            where TEntity : class
        {
            foreach (var property in properties)
            {
                //var propertyName = System.Web.Mvc.ExpressionHelper.GetExpressionText(property);
                base.Entry(item).Property(property).IsModified = true;
            }

            //base.Entry<TEntity>(item).State = System.Data.EntityState.Modified;
        }

        public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current)
            where TEntity : class
        {
            //if it is not attached, attach original and set current values
            base.Entry<TEntity>(original).CurrentValues.SetValues(current);
        }

        public void Commit()
        {
            base.SaveChanges();
        }


        public void CommitAndRefreshChanges()
        {
            bool saveFailed = false;

            do
            {
                try
                {
                    base.SaveChanges();

                    saveFailed = false;

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;

                    ex.Entries.ToList()
                              .ForEach(entry =>
                              {
                                  entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                              });

                }
            } while (saveFailed);
        }

        public void RollbackChanges()
        {
            // set all entities in change tracker
            // as 'unchanged state'
            base.ChangeTracker.Entries()
                              .ToList()
                              .ForEach(entry => entry.State = System.Data.Entity.EntityState.Unchanged);
        }

        public IEnumerable<TEntity> ExecuteQuery<TEntity>(string sqlQuery, params object[] parameters)
        {
            return base.Database.SqlQuery<TEntity>(sqlQuery, parameters);
        }

        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        #endregion

        #region DbContext Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            //Remove unused conventions
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Add entity configurations in a structured way using 'TypeConfiguration’ classes
            modelBuilder.Configurations.Add(new TableUniqueTypeConfiguration());

            /**
             * Hay un detalle en el funcionamiento de los RowVersion, cuando se actualiza un registro no se cambia al siguiente
             * Rowversion del registro, si no se actualiza al siguiente Rowversion de todos los registros
             * */
            //modelBuilder.Entity<TCRM_AREAFORMACION>().Property(g => g.RowVersion).IsRowVersion();
            modelBuilder.Entity<MoOne_TableUnique>().Property(g => g.RowVersion).IsRowVersion().IsConcurrencyToken(false);
            modelBuilder.Entity<MoOne_TableUnique1>().Property(g => g.RowVersion).IsRowVersion().IsConcurrencyToken(false);

            
            //Registrando con el esquema
            //modelBuilder.Entity<TDO_Personal>().ToTable("TDO_Personal", schemaName: "Do");
        }

        #endregion
    }
}
