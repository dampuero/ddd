
namespace Mo.Dominio.Entidades
{
    using Core.Dominio.Entidades;
    
    public class MoTwo_TableTest : Entity
    {
        #region Atributos
        
		public int dato1 { get; set; }
		public int dato2 { get; set; }
		public string dato3 { get; set; }
		public string dato4 { get; set; }

        public byte[] RowVersion { get; set; }

        #endregion
    }
}      
