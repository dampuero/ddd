﻿
namespace Core.Dominio
{
    using Core.Dominio.Entidades;
    using Core.Dominio.Specification;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    /// <summary>
    /// Interface para implementar el Patron Repositorio
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IDisposable
        where TEntity : Entity
    {
        /// <summary>
        /// Unidad de Trabajo
        /// </summary>
        IUnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Anyadir entidad al Repositorio
        /// </summary>
        /// <param name="item"></param>
        TEntity Add(TEntity item);

        /// <summary>
        /// Eliminar Item 
        /// </summary>
        /// <param name="item"></param>
        TEntity Remove(TEntity item);

        /// <summary>
        /// FUNCION QUE PONE EN "0" EL ESTADO PARA ELIMINAR UNA ENTIDAD
        /// </summary>
        /// <param name="item"></param>
        void RemoveEstado(TEntity item);

        /// <summary>
        /// Establecer ITem a modificar
        /// </summary>
        /// <param name="item"></param>
        TEntity Modify(TEntity item);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <param name="properties"></param>
        void Modify(TEntity item, /*Expression<Func<TEntity, object>>[]*/IList<string> properties);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        void TrackItem(TEntity item);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        bool isDetached(TEntity item);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="persisted"></param>
        /// <param name="current"></param>
        void Merge(TEntity persisted, TEntity current);

        /// <summary>
        /// Obtener una entidad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity Get(Guid id);

        /// <summary>
        /// Obtener una lista de Entidades
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="specification"></param>
        /// <returns></returns>
        IEnumerable<TEntity> AllMatching(ISpecification<TEntity> specification);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="KProperty"></typeparam>
        /// <param name="pageIndex"></param>
        /// <param name="pageCount"></param>
        /// <param name="skip"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetPaged<KProperty>(int pageIndex, int pageCount, int skip,
            Expression<Func<TEntity, KProperty>> orderByExpression, bool ascending, GridFilters filter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="orders"></param>
        /// <param name="skip"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetPaged(IList<FilterInfo> filters, IList<OrderInfo> orders, int skip, int pageCount);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        int CountAll(IList<FilterInfo> filters);

        /// <summary>
        /// Cuenta todas las entidades
        /// </summary>
        /// <returns></returns>
        int CountAll();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        int CountAll(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        IEnumerable<TEntity> GetFiltered(Expression<Func<TEntity, bool>> filter);
        IEnumerable<TEntity> GetFiltered(IEnumerable<System.Linq.Expressions.Expression<Func<TEntity, bool>>> filters);
    }
}
