﻿
namespace Core.Dominio
{
    using Core.Dominio.Entidades;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;

    public class ServiceAbstract<TEntity> : IService<TEntity> 
        where TEntity : Entity
    {
        #region Members

        private Dominio.IRepository<TEntity> _repository;

        #endregion

        #region Constructor

        public ServiceAbstract() { }

        protected void setRepository(Dominio.IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        #endregion

        public TEntity Get(Guid id)
        {
            if (id == null) throw new ArgumentNullException("Identificador null");
            return _repository.Get(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }
        
        public IEnumerable<TEntity> GetPaged(IList<FilterInfo> filters, IList<OrderInfo> orders,
            int skip, int pageCount)
        {
            if (filters == null || orders == null) throw new ArgumentNullException("GetPaged");
            return _repository.GetPaged(filters, orders, skip, pageCount);
        }

        public int CountAll(IList<FilterInfo> filters)
        {
            if (filters == null || filters.Count <= 0) throw new ArgumentNullException("CountAll");
            return _repository.CountAll(filters);
        }

        public TEntity Insert(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("Insert");

            entity.GenerateNewIdentity();
            var rpta = _repository.Add(entity);
            _repository.UnitOfWork.Commit();
            return rpta;
        }

        public TEntity Update(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("Insert");
            
            _repository.Modify(entity);
            _repository.UnitOfWork.Commit();
            return entity;
        }
        
        public TEntity DeleteVirtual(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("DeleteVirtual");

            _repository.Modify(entity);
            _repository.UnitOfWork.Commit();
            return entity;
        }

        public TEntity Delete(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException("Delete");

            return _repository.Remove(entity);
        }

        public void commit()
        {
            _repository.UnitOfWork.Commit();
        }
    }
}
