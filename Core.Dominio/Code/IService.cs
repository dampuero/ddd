﻿
namespace Core.Dominio
{
    using Core.Dominio.Entidades;
    using System;
    using System.Collections.Generic;

    public interface IService<TEntity> where TEntity : Entity
    {
        TEntity Get(Guid id);

        TEntity Insert(TEntity entity);

        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> GetPaged(IList<FilterInfo> filters, IList<OrderInfo> orders, 
            int skip, int pageCount);

        int CountAll(IList<FilterInfo> filters);

        TEntity Update(TEntity entity);

        TEntity Delete(TEntity entity);

        TEntity DeleteVirtual(TEntity entity);

        void commit();
    }
}
