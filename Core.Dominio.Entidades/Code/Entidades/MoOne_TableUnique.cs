
namespace Mo.Dominio.Entidades
{
    using Core.Dominio.Entidades;
    using System;
    using System.Collections.Generic;

    public class MoOne_TableUnique : Entity
    {
        #region Atributos
        
		public int dato1 { get; set; }
		public int dato2 { get; set; }
		public string dato3 { get; set; }
		public string dato4 { get; set; }

        public byte[] RowVersion { get; set; }

        #endregion

    }
}      
