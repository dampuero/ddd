﻿
namespace ModuloOne.Aplicacion.Dto
{
    public class MoOne_TableUniqueDto
    {
        public string Id { get; set; }
        public int dato1 { get; set; }
        public int dato2 { get; set; }
        public string dato3 { get; set; }
        public string dato4 { get; set; }

        public string RowVersion { get; set; }
    }
}
