﻿
namespace webMain.Scode
{
    using Microsoft.Owin.Security.OAuth;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;

    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identy = new ClaimsIdentity(context.Options.AuthenticationType);
            if(context.UserName == "admin" && context.Password == "admin")
            {
                identy.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                identy.AddClaim(new Claim("username", "admin"));
                identy.AddClaim(new Claim(ClaimTypes.Name, "Diego admin"));
                context.Validated(identy);

            }else if (context.UserName == "user" && context.Password == "user")
            {
                identy.AddClaim(new Claim(ClaimTypes.Role, "user"));
                identy.AddClaim(new Claim("username", "user"));
                identy.AddClaim(new Claim(ClaimTypes.Name, "Diego user"));
                context.Validated(identy);

            } else
            {
                context.SetError("Invalid_grant","Provider user errp");
                return;
            }
        }
    }
}