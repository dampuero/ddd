﻿
namespace webMain.Controllers
{
    using Mo.Dominio.Entidades;
    using ModuloOne.Aplicacion.Dto;
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Http;
    using System.Linq;
    using Mo.Dominio.IServicios;
    using System;
    using AutoMapper;
    using Core.Dominio.Entidades;
    using System.Security.Claims;
    using FluentValidation;
    using log4net;

    /// <summary>
    /// Controlador ejemplo que contiene las funciones necesarias para el correcto funcionamiento
    /// de un CRUD
    /// </summary>
    public class ValuesController : ApiController
    {
        private readonly MoOne_ITableUniqueService serviceOne;
        private readonly IValidator<MoOne_TableUniqueDto> _validatorTableUniqueDto;

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="servicio"></param>
        public ValuesController(MoOne_ITableUniqueService servicio, IValidator<MoOne_TableUniqueDto> validator)
        {
            if(servicio == null) throw new ArgumentNullException("MoOne_ITableUniqueService");
            if(validator == null) throw new ArgumentNullException("IValidator<MoOne_TableUniqueDto>");

            serviceOne = servicio;
            _validatorTableUniqueDto = validator;
        }
        /// <summary>
        /// Devuelve un objeto Dto a partir de un Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public IHttpActionResult Get(Guid id)
        {
            try
            {
                return Ok(Mapper.Map<MoOne_TableUnique, MoOne_TableUniqueDto>(serviceOne.Get(id)));
            }
            catch (ArgumentNullException ex)
            {
                Log.Error("No se encontro el elemento con el valor indicado", ex);
                return BadRequest();
            }
            catch (AutoMapperMappingException ex)
            {
                Log.Error("Error en el mapeo", ex);
                return BadRequest();
            }
            catch (KeyNotFoundException ex)
            {
                Log.Error("No se encontro el elemento con el valor indicado", ex);
                return NotFound();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest();
            }
        }
        /// <summary>
        /// Devuelve todos los elementos de una entidad. No es recomendable su uso a menos
        /// que se tenga la seguridad de que la cantidad de elementos es realmente pequeña
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IHttpActionResult GetAll()
        {
            try
            {
                var list = serviceOne.GetAll();
                var rpta = Mapper.Map<IEnumerable<MoOne_TableUnique>, IEnumerable<MoOne_TableUniqueDto>>
                    (list);
                return Ok(rpta);

            }
            catch (AutoMapperMappingException ex)
            {
                Log.Error("Error en el mapeo", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest(ex.ToString());
            }
        }
        /// <summary>
        /// Funcion que retorna elementos  de una entidad de acuerdo a los filtros que se le envia,
        /// el orden que se indica y la cantidad especificada de acuerdo a length y desde startIndex
        /// </summary>
        /// <param name="filtros"></param>
        /// <param name="orden"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult GetPaged(PagedItem pagedItem)
        {
            try
            {
                return Ok(Mapper.Map<IEnumerable<MoOne_TableUnique>, IEnumerable<MoOne_TableUniqueDto>>
                    (serviceOne.GetPaged(pagedItem.filtros, pagedItem.orden, pagedItem.startIndex,
                    pagedItem.length)));
            }
            catch (ArgumentNullException ex)
            {
                Log.Error("No se acepta valor nulo", ex);
                return BadRequest();
            }
            catch (AutoMapperMappingException ex)
            {
                Log.Error("Error en el mapeo", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest();
            }
        }
        /// <summary>
        /// Cuenta el nro de entidades al aplicar filters
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public IHttpActionResult CountAll(IList<FilterInfo> filters)
        {
            try
            {
                return Ok(serviceOne.CountAll(filters));

            }
            catch (ArgumentNullException ex)
            {
                Log.Error("No se acepta valor nulo", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        //[Authorize]
        [AllowAnonymous]
        public IHttpActionResult Post([FromBody]MoOne_TableUniqueDto entityDto)
        {
            try
            {
                _validatorTableUniqueDto.ValidateAndThrow(entityDto);

                var entity = Mapper.Map<MoOne_TableUniqueDto, MoOne_TableUnique>(entityDto);
                var identity = (ClaimsIdentity)User.Identity;
                entity.AsNew(/*identity.Name*/"admin");

                return Ok(Mapper.Map<MoOne_TableUnique, MoOne_TableUniqueDto>(
                    serviceOne.Insert(entity)));
            }
            catch (ArgumentNullException ex)
            {
                Log.Error("No se acepta valor nulo", ex);
                return BadRequest();
            }
            catch (ValidationException ex)
            {
                Log.Error("Error de validacion", ex);
                return BadRequest(ex.ToString());
            }
            catch (AutoMapperMappingException ex)
            {
                Log.Error("Error en el mapeo", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="entity"></param>
        //[Authorize]
        [AllowAnonymous]
        public IHttpActionResult Put([FromBody]MoOne_TableUniqueDto entityDto)
        {
            try
            {
                _validatorTableUniqueDto.ValidateAndThrow(entityDto);

                var entity = Mapper.Map<MoOne_TableUniqueDto, MoOne_TableUnique>(entityDto);
                var identity = (ClaimsIdentity)User.Identity;
                entity.AsUpdate(/*identity.Name*/"admin");

                return Ok(Mapper.Map<MoOne_TableUnique, MoOne_TableUniqueDto>(
                    serviceOne.Update(entity)));
            }
            catch (ArgumentNullException ex)
            {
                Log.Error("No se acepta valor nulo", ex);
                return BadRequest();
            }
            catch (ValidationException ex)
            {
                Log.Error("Error de validacion", ex);
                return BadRequest(ex.ToString());
            }
            catch (KeyNotFoundException ex)
            {
                Log.Error("No se encontro el elemento con el valor indicado", ex);
                return NotFound();
            }
            catch (AutoMapperMappingException ex)
            {
                Log.Error("Error en el mapeo", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest();
            }
        }
        /// <summary>
        /// Elimina de forma logica la entidad especificada
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        //[Authorize]
        [AllowAnonymous]
        public IHttpActionResult Delete(MoOne_TableUniqueDto entityDto)
        {
            try
            {
                _validatorTableUniqueDto.ValidateAndThrow(entityDto);

                var entity = Mapper.Map<MoOne_TableUniqueDto, MoOne_TableUnique>(entityDto);
                var identity = (ClaimsIdentity)User.Identity;
                entity.AsDelete(/*identity.Name*/"admin");

                return Ok(Mapper.Map<MoOne_TableUnique, MoOne_TableUniqueDto>(serviceOne.DeleteVirtual(entity)));
            }
            catch (ArgumentNullException ex)
            {
                Log.Error("No se acepta valor nulo", ex);
                return BadRequest();
            }
            catch (ValidationException ex)
            {
                Log.Error("Error de validacion", ex);
                return BadRequest(ex.ToString());
            }
            catch (KeyNotFoundException ex)
            {
                Log.Error("No se encontro el elemento con el valor indicado", ex);
                return NotFound();
            }
            catch (AutoMapperMappingException ex)
            {
                Log.Error("Error en el mapeo", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Error", ex);
                return BadRequest();
            }
        }

        /// <summary>
        /// Elimina de forma logica la entidad a la que está asociado el Id
        /// </summary>
        /// <param name="id"></param>
        [AllowAnonymous]
        public void Delete(int id)
        {
        }

        // Esta funcion no corresponde a las funciones necesarias
        //[Authorize]
        [AllowAnonymous]
        public MoOne_TableUniqueDto insertCustom([FromBody]MoOne_TableUniqueDto entityDto)
        {
            
            try
            {
                _validatorTableUniqueDto.ValidateAndThrow(entityDto);

                var entity = Mapper.Map<MoOne_TableUniqueDto, MoOne_TableUnique>(entityDto);
                var identity = (ClaimsIdentity)User.Identity;
                entity.AsNew(/*identity.Name*/"admin");

                return Mapper.Map<MoOne_TableUnique, MoOne_TableUniqueDto>(
                    serviceOne.insertCustom(entity));
            }
            catch (ValidationException)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            catch (KeyNotFoundException)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // Esta funcion no corresponde a las funciones necesarias
        [AllowAnonymous]
        public IEnumerable<MoOne_TableUnique> GetAllCustom()
        {
            try
            {
                IEnumerable<MoOne_TableUnique> a = this.serviceOne.GetAll();
                IList<MoOne_TableUnique> aa = a.ToList<MoOne_TableUnique>();
                return aa;
                //return this.servicio.GetAll();
            }
            catch (KeyNotFoundException)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        // Esta funcion no corresponde a las funciones necesarias
        [AllowAnonymous]
        public IEnumerable<MoOne_TableUniqueDto> GetAllCustom1
            (IList<FilterInfo> filtros, IList<OrderInfo> orden,
            int startIndex, int length)
        {
            try
            {
                // filters 
                IList<FilterInfo> filters = new List<FilterInfo>
                {
                    new FilterInfo() { Logical = Logical.AND, PropertyName = "dato3", Value = "3333", Operator = Operator.Equals },
                    new FilterInfo() { Logical = Logical.AND, PropertyName = "dato4", Value = "4", Operator = Operator.Contains }
                };
                // orders
                IList<OrderInfo> order = new List<OrderInfo>();
                order.Add(new OrderInfo()
                {
                    Index = 0,
                    OrderType = OrderType.DESC,
                    Property = "dato3"
                });
                int StartIndex = 0;
                int Length = 3;
                return Mapper.Map<IEnumerable<MoOne_TableUnique>, IEnumerable<MoOne_TableUniqueDto>>
                    (serviceOne.GetPaged(filters, order, StartIndex, Length));

            }
            catch (KeyNotFoundException)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }
    }
}
