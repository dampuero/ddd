﻿
namespace webMain
{
    using AutoMapper;
    using Mo.Dominio.Entidades;
    using ModuloOne.Aplicacion.Dto;
    using System;

    public class MapperConfig
    {
        public static void RegisterMapper()
        {
            // Mappes de la aplicacion
            
            Mapper.Initialize(cfg =>
            {
                // Mapas para convertir los String a Guid y viceversa
                cfg.CreateMap<string, Guid>().ConvertUsing((s) =>
                {
                    if (s == null)
                        return Guid.Empty;
                    return new Guid(s);
                });
                cfg.CreateMap<Guid, string>().ConvertUsing((s) => 
                {
                    if (s == null)
                        return string.Empty;
                    return Convert.ToString(s);
                });

                //Mappas para convertir arreglos de bytes a String y viceversa (RowVersion)
                cfg.CreateMap<string, Byte[]>()
                .ConvertUsing((s) =>
                {
                    if (s == null)
                        s = string.Empty;
                    int NumberChars = s.Length;
                    byte[] bytes = new byte[NumberChars / 2];//9
                    for (int i = 0; i < NumberChars; i += 2)
                        bytes[i / 2] = Convert.ToByte(s.Substring(i, 2), 16);
                    return bytes;
                });
                cfg.CreateMap<Byte[], string>()
                .ConvertUsing((s) =>
                {
                    System.Text.StringBuilder hex = new System.Text.StringBuilder(s.Length * 2);
                    foreach (byte b in s)
                        hex.AppendFormat("{0:x2}", b);
                    return hex.ToString();
                });

                /**
                * Otros Mapeadores
                */
                /// Mapeadores para el modulo one
                cfg.CreateMap<MoOne_TableUnique, MoOne_TableUniqueDto>()
                    .IgnoreAllPropertiesWithAnInaccessibleSetter()
                    .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
                    .ForAllMembers(opt => opt.Condition(src => src != null));
                    
                cfg.CreateMap<MoOne_TableUniqueDto, MoOne_TableUnique>()
                    .IgnoreAllPropertiesWithAnInaccessibleSetter()
                    .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
                    .ForAllMembers(opt => opt.Condition(src => src != null));

            });
        }
    }
}