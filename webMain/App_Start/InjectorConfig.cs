﻿namespace webMain
{
    using System.Web.Http;
    using SimpleInjector;
    using SimpleInjector.Lifestyles;
    using SimpleInjector.Integration.WebApi;
    using Mo.Dominio.IRepositorios;
    using ModuleOne.Persistencia.Repositorios;
    using ModuleOne.Persistencia.UnidadDeTrabajo;
    using Mo.Dominio.IServicios;
    using Mo.Dominio.Servicios;
    using ModuleTwo.Persistencia.Repositorios;
    using ModuleTwo.Persistencia.UnidadDeTrabajo;
    using FluentValidation;
    using ModuloOne.Aplicacion.Dto;
    using ModuleOne.Transversal;

    public class InjectorConfig
    {
        public static void RegisterInjector()
        {
            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            /**
             * Registrar las Unidades de Trabajo 
             */
            //
            container.Register<MoOne_UnitOfWork, MoOne_UnitOfWork>(Lifestyle.Scoped);

            container.Register<MoTwo_UnitOfWork, MoTwo_UnitOfWork>(Lifestyle.Scoped);

            /**
             * Registrar las interfaces del modulo ModuleOne 
             */
            // Register your types, for instance using the scoped lifestyle:
            container.Register<MoOne_ITableUniqueRepository, MoOne_TableUniqueRepository>(Lifestyle.Scoped);

            container.Register<MoOne_ITableUniqueService, MoOne_TableUniqueService>(Lifestyle.Scoped);

            /**
             * Registrar las interfaces del modulo ModuleTwo 
             */
            //
            container.Register<MoTwo_ITableTestRepository, MoTwo_TableTestRepository>(Lifestyle.Scoped);

            container.Register<MoTwo_ITableTestService, MoTwo_TableTestService>(Lifestyle.Scoped);

            /**
             * Registrar Los Validadores 
             */
            //
            container.Register<IValidator<MoOne_TableUniqueDto>, MoOne_TableUniqueDtoValidator>(Lifestyle.Scoped);

            /**
             * Ultimas configuraciones del container 
             */
            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}