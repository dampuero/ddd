﻿
namespace Core.Persistencia
{
    using Core.Dominio;
    using Core.Dominio.Entidades;
    using Core.Dominio.Specification;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.Entity;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : Entity
    {
        #region Members

        IQueryableUnitOfWork _UnitOfWork;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor que crea una instancia de un repositorio
        /// </summary>
        /// <param name="unitOfWork">Asociado a una Unidad de Trabajo</param>
        public Repository(IQueryableUnitOfWork unitOfWork)
        {
            if (unitOfWork == (IUnitOfWork)null)
                throw new ArgumentNullException("unitOfWork");

            _UnitOfWork = unitOfWork;
        }

        #endregion

        #region IRepository Members

        /// <summary>
        /// 
        /// </summary>
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _UnitOfWork;
            }
        }

        /// <summary>
        /// Anyade una actividad al Repositorio
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual TEntity Add(TEntity item)
        {

            if (item != (TEntity)null)
            {
                var seter = GetSet();
                TEntity aa = seter.Add(item);
                return aa;
            }
            else
            {
                return null;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"> </param>
        public virtual TEntity Remove(TEntity item)
        {
            if (item != (TEntity)null)
            {
                //attach item if not exist
                _UnitOfWork.Attach(item);

                //eliminando
                return GetSet().Remove(item);
            }
            else
            {
                /*LoggerFactory.CreateLog()
                          .LogInfo(Messages.info_CannotRemoveNullEntity, typeof(TEntity).ToString());*/
                return null;
            }
        }

        /// <summary>
        /// FUNCION QUE PONE EN "0" EL ESTADO PARA ELIMINAR UNA ENTIDAD
        /// </summary>
        /// <param name="item"></param>
        public virtual void RemoveEstado(TEntity item)
        {
            IList<string> lista = new List<string>();
            item.ESTADO = false;
            lista.Add("ESTADO");
            if (item != (TEntity)null)
            {
                _UnitOfWork.Attach<TEntity>(item);
                _UnitOfWork.SetModified(item, lista);
            }
            else
            {
                /*LoggerFactory.CreateLog()
                          .LogInfo(Messages.info_CannotRemoveNullEntity, typeof(TEntity).ToString());*/
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"> </param>
        public virtual void TrackItem(TEntity item)
        {
            if (item != (TEntity)null)
                _UnitOfWork.Attach<TEntity>(item);
            else
            {
                /*LoggerFactory.CreateLog()
                          .LogInfo(Messages.info_CannotRemoveNullEntity, typeof(TEntity).ToString());*/
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"> </param>
        public virtual bool isDetached(TEntity item)
        {
            if (item != (TEntity)null)
                return _UnitOfWork.isDetached<TEntity>(item);
            else
            {
                /*LoggerFactory.CreateLog()
                          .LogInfo(Messages.info_CannotRemoveNullEntity, typeof(TEntity).ToString());*/
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public virtual TEntity Modify(TEntity item)
        {
            if (item != (TEntity)null)
            {
                _UnitOfWork.SetModified(item);
                return item;
            }
            else
            {
                /*LoggerFactory.CreateLog()
                          .LogInfo(Messages.info_CannotRemoveNullEntity, typeof(TEntity).ToString());*/
                return null;
            }
        }

        public virtual void Modify(TEntity item, /*Expression<Func<TEntity, object>>[]*/
            IList<string> properties)
        {
            if (item != (TEntity)null)
            {
                _UnitOfWork.Attach<TEntity>(item);
                _UnitOfWork.SetModified(item, properties);
            }
            else
            {
                /*LoggerFactory.CreateLog()
                          .LogInfo(Messages.info_CannotRemoveNullEntity, typeof(TEntity).ToString());*/
            }
        }

        public virtual void Modify2(TEntity item, /*Expression<Func<TEntity, object>>[]*/
            IList<string> properties)
        {
            if (item != (TEntity)null)
            {
                _UnitOfWork.SetModified(item, properties);
            }
        }


        /// <summary>
        /// Obtener una Entidad
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity Get(Guid id)
        {
            if (id != null && id != Guid.Empty)
            {
                var rpta = GetSet().Find(id);
                if (rpta == null) throw new KeyNotFoundException("El Identificador enviado no existe actualmente"
                    + id.ToString());
                return rpta;
            }
            else
                throw new ArgumentNullException("Parametro null");
        }

        /// <summary>
        /// Obtener una lista de todas las actividades
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            return GetSet().Where(e => e.ESTADO == true).AsNoTracking();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="orders"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public IEnumerable<TEntity> GetPaged(IList<FilterInfo> filters, IList<OrderInfo> orders,
            int skip, int pageCount)
        {
            var queryable = GetSet().Where(e => e.ESTADO == true); //_repository.GetStudents();

            var predicate = SimpleHandler<TEntity>.BuildPredicate(filters);
            var filtered = queryable;
            if (filters.Count > 0 )
                if(predicate != null)
                    filtered = queryable.Where(predicate);
                else
                    return Enumerable.Empty<TEntity>();
            
            //if (orders.Count > 0)
            var ordered = OrderHelper.GetOrderedQueryable<TEntity>(filtered, orders);
            
            var pagination = ordered.Skip(skip).Take(pageCount).AsNoTracking();
            return pagination;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="specification"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> AllMatching(ISpecification<TEntity> specification)
        {
            return GetSet().Where(specification.SatisfiedBy()).AsNoTracking();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="KProperty"></typeparam>
        /// <param name="pageIndex"></param>
        /// <param name="pageCount"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetPaged<KProperty>(int pageIndex, int pageCount, int skip,
            System.Linq.Expressions.Expression<Func<TEntity, KProperty>> orderByExpression, bool ascending,
            GridFilters filter)
        {
            var set = GetSet().Where(e => e.ESTADO == true).AsNoTracking();

            if (ascending)
            {
                return set.OrderBy(orderByExpression)
                          //.Skip(pageCount * pageIndex)
                          .Skip(skip)
                          .Take(pageCount);
            }
            else
            {
                return set.OrderByDescending(orderByExpression)
                          //.Skip(pageCount * pageIndex)
                          .Skip(skip)
                          .Take(pageCount);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public int CountAll(IList<FilterInfo> filters)
        {
            var predicate = SimpleHandler<TEntity>.BuildPredicate(filters);
            if (predicate != null)
            {
                // queryable
                var queryable = GetSet().Where(e => e.ESTADO == true); //_repository.GetStudents();

                // filters
                var filtered = queryable.Where(predicate);

                // result (call the linq)
                var result = filtered;

                return result.Count();
            }
            return -1;
        }
        /// <summary>
        /// Cuenta todas las entidades
        /// </summary>
        /// <returns></returns>
        public virtual int CountAll()
        {
            //return GetSet().Count();
            return GetSet().Where(e => e.ESTADO == true).Count();
        }

        public virtual int CountAll(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter)
        {
            //return GetSet().Count();
            return GetSet().Where(e => e.ESTADO == true).Where(filter).Count();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetFiltered(
            System.Linq.Expressions.Expression<Func<TEntity, bool>> filter)
        {
            return GetSet().Where(e => e.ESTADO == true).Where(filter).AsNoTracking();
        }
        public virtual IEnumerable<TEntity> GetFiltered(
            IEnumerable<System.Linq.Expressions.Expression<Func<TEntity, bool>>> filters)
        {
            var rpta = GetSet().Where(e => e.ESTADO == true);
            foreach (var filter in filters)
            {
                rpta = rpta.Where(filter);
            }
            return rpta.AsNoTracking();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="persisted"></param>
        /// <param name="current"></param>
        public virtual void Merge(TEntity persisted, TEntity current)
        {
            _UnitOfWork.ApplyCurrentValues(persisted, current);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            if (_UnitOfWork != null)
                _UnitOfWork.Dispose();
        }

        #endregion

        #region Private Methods

        IDbSet<TEntity> GetSet()
        {
            return _UnitOfWork.CreateSet<TEntity>();
        }

        
        #endregion



    }
}
