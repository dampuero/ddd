use test1;
GO


CREATE TABLE [dbo].[table_unique](
	[id] [uniqueidentifier] NOT NULL,
	[dato1] [int] NULL,
	[dato2] [int] NULL,
	[dato3] [nchar](100) NULL,
	[dato4] [nchar](100) NULL,
	[estado] [bit] NOT NULL,
	[disponibilidad] [bit] NOT NULL,
	[fecha_creacion] [int] NOT NULL,
	[fecha_modificacion] [int] NOT NULL,
	[user_creacion] [nchar](25) NOT NULL,
	[user_modificacion] [nchar](25) NOT NULL,
	[rowversion] [timestamp] NOT NULL,
	CONSTRAINT [PK_table_unique] PRIMARY KEY CLUSTERED (id)
)

CREATE SCHEMA ModuleTwo

CREATE TABLE [ModuleTwo].[table_test](
	[id] [uniqueidentifier] NOT NULL,
	[dato1] [int] NULL,
	[dato2] [int] NULL,
	[dato3] [nchar](100) NULL,
	[dato4] [nchar](100) NULL,
	[estado] [bit] NOT NULL,
	[disponibilidad] [bit] NOT NULL,
	[fecha_creacion] [int] NOT NULL,
	[fecha_modificacion] [int] NOT NULL,
	[user_creacion] [nchar](25) NOT NULL,
	[user_modificacion] [nchar](25) NOT NULL,
	[rowversion] [timestamp] NOT NULL,
	CONSTRAINT [PK_table_unique] PRIMARY KEY CLUSTERED (id)
)
GO
