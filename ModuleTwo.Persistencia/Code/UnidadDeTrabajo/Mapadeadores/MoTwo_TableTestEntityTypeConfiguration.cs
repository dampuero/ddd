
namespace ModuleTwo.Persistencia.UnidadDeTrabajo.Mapeadores
{
    
    using Mo.Dominio.Entidades;
    using System.Data.Entity.ModelConfiguration;
    
    class TableTestTypeConfiguration : EntityTypeConfiguration<MoTwo_TableTest>
    {
        public TableTestTypeConfiguration()
        {
            //key and properties
            ToTable("Table_test");
            this.HasKey(ba => ba.Id);
            			this.Property(ba => ba.dato1);
			this.Property(ba => ba.dato2);
			this.Property(ba => ba.dato3);
			this.Property(ba => ba.dato4);

            this.Property(ba => ba.RowVersion);
        }
    }
}
