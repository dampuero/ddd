
namespace ModuleTwo.Persistencia.UnidadDeTrabajo.Mapeadores
{
    
    using Mo.Dominio.Entidades;
    using System.Data.Entity.ModelConfiguration;
    
    class TableTest1TypeConfiguration : EntityTypeConfiguration<MoTwo_TableTest1>
    {
        public TableTest1TypeConfiguration()
        {
            //key and properties
            ToTable("Table_test1");
            this.HasKey(ba => ba.Id);
            			this.Property(ba => ba.dato1);
			this.Property(ba => ba.dato2);
			this.Property(ba => ba.dato3);
			this.Property(ba => ba.dato4);

            this.Property(ba => ba.RowVersion);
        }
    }
}
