﻿

using Core.Persistencia;
using System.Data.Entity;
using System;
using System.Collections.Generic;
using Mo.Dominio.Entidades;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using ModuleTwo.Persistencia.UnidadDeTrabajo.Mapeadores;

namespace ModuleTwo.Persistencia.UnidadDeTrabajo
{
    public class MoTwo_UnitOfWork : DbContext, IQueryableUnitOfWork
    {
        public MoTwo_UnitOfWork()
            : base("ModuleTwo.Persistencia.UnidadDeTrabajo.MoTwo_UnitOfWork")
        {
            Database.SetInitializer<MoTwo_UnitOfWork>(null);
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }


        #region IDbSet Miembros

        IDbSet<MoTwo_TableTest> _tableTest;
        public IDbSet<MoTwo_TableTest> TableTest
        {
            get
            {
                if (_tableTest == null)
                    _tableTest = base.Set<MoTwo_TableTest>();

                return _tableTest;
            }
        }

        IDbSet<MoTwo_TableTest1> _tableTest1;
        public IDbSet<MoTwo_TableTest1> TableTest1
        {
            get
            {
                if (_tableTest1 == null)
                    _tableTest1 = base.Set<MoTwo_TableTest1>();

                return _tableTest1;
            }
        }


        #endregion
        #region IQueryableUnitOfWork Miembros

        public DbSet<TEntity> CreateSet<TEntity>()
            where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public void Attach<TEntity>(TEntity item)
            where TEntity : class
        {
            //attach and set as unchanged
            base.Entry<TEntity>(item).State = System.Data.Entity.EntityState.Unchanged;
        }

        public bool isDetached<TEntity>(TEntity item)
            where TEntity : class
        {
            //attach and set as unchanged
            return base.Entry<TEntity>(item).State == System.Data.Entity.EntityState.Detached;
        }

        public void SetModified<TEntity>(TEntity item)
            where TEntity : class
        {
            //this operation also attach item in object state manager
            base.Entry<TEntity>(item).State = System.Data.Entity.EntityState.Modified;
            //base.Entry(item).Property(x => x.).IsModified = false;
        }

        public void SetModified<TEntity>(TEntity item,
            /*System.Linq.Expressions.Expression<Func<TEntity, object>>[]*/IList<string> properties)
            where TEntity : class
        {
            foreach (var property in properties)
            {
                //var propertyName = System.Web.Mvc.ExpressionHelper.GetExpressionText(property);
                base.Entry(item).Property(property).IsModified = true;
            }

            //base.Entry<TEntity>(item).State = System.Data.EntityState.Modified;
        }

        public void ApplyCurrentValues<TEntity>(TEntity original, TEntity current)
            where TEntity : class
        {
            //if it is not attached, attach original and set current values
            base.Entry<TEntity>(original).CurrentValues.SetValues(current);
        }

        public void Commit()
        {
            base.SaveChanges();
        }


        public void CommitAndRefreshChanges()
        {
            bool saveFailed = false;

            do
            {
                try
                {
                    base.SaveChanges();

                    saveFailed = false;

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;

                    ex.Entries.ToList()
                              .ForEach(entry =>
                              {
                                  entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                              });

                }
            } while (saveFailed);
        }

        public void RollbackChanges()
        {
            // set all entities in change tracker
            // as 'unchanged state'
            base.ChangeTracker.Entries()
                              .ToList()
                              .ForEach(entry => entry.State = System.Data.Entity.EntityState.Unchanged);
        }

        public IEnumerable<TEntity> ExecuteQuery<TEntity>(string sqlQuery, params object[] parameters)
        {
            return base.Database.SqlQuery<TEntity>(sqlQuery, parameters);
        }

        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        #endregion

        #region DbContext Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Remove unused conventions
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Add entity configurations in a structured way using 'TypeConfiguration’ classes
            modelBuilder.Configurations.Add(new TableTestTypeConfiguration());

            //modelBuilder.Configurations.Add(new TCRM_ReporteCoberturaTypeConfiguration());

            modelBuilder.Entity<MoTwo_TableTest>().Property(g => g.RowVersion).IsRowVersion();
            modelBuilder.Entity<MoTwo_TableTest1>().Property(g => g.RowVersion).IsRowVersion();


            //Registrando con el esquema
            modelBuilder.Entity<MoTwo_TableTest>().ToTable("Table_test", schemaName: "ModuleTwo");
            modelBuilder.Entity<MoTwo_TableTest1>().ToTable("Table_test1", schemaName: "ModuleTwo");
        }

        #endregion
    }
}
