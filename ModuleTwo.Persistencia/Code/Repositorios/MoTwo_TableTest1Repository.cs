
namespace ModuleTwo.Persistencia.Repositorios
{
    using System;
    using Core.Persistencia;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IRepositorios;
    using ModuleTwo.Persistencia.UnidadDeTrabajo;
    
    public class MoTwo_TableTest1Repository : Repository<MoTwo_TableTest1>, MoTwo_ITableTest1Repository
    {
        public MoTwo_TableTest1Repository(MoTwo_UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        //Generate test Method 
        public MoTwo_TableTest1 getCustom(Guid id)
        {
            if (id != Guid.Empty)
                return base.Get(id);
            else
                return null;
        }
    }
}        
           