
namespace ModuleTwo.Persistencia.Repositorios
{
    using System;
    using Core.Persistencia;
    using Mo.Dominio.Entidades;
    using Mo.Dominio.IRepositorios;
    using ModuleTwo.Persistencia.UnidadDeTrabajo;
    
    public class MoTwo_TableTestRepository : Repository<MoTwo_TableTest>, MoTwo_ITableTestRepository
    {
        public MoTwo_TableTestRepository(MoTwo_UnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        //Generate test Method 
        public MoTwo_TableTest getCustom(Guid id)
        {
            if (id != Guid.Empty)
                return base.Get(id);
            else
                return null;
        }
    }
}        
           